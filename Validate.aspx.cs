﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;

public partial class Validate : System.Web.UI.Page
{
    //Change below to false for production deploy
    const bool DEBUG_LOCAL = false;

    //For authorization
    public struct ACSData
    {
        public string access_token { get; set; }

        public string token_type { get; set; }

        public string refresh_token { get; set; }

        public string expires_in { get; set; }

        public string error { get; set; }


        public string error_description { get; set; }
    }

    public struct UserData
    {
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Int64 id { get; set; }

    }

    public class Collaborator
    {
        public Int64 sharedBy { get; set; }
        public string created { get; set; }
        public Int64 userId { get; set; }
    }

    public class Sheet
    {
        public Sheet() { collaborators = new List<Collaborator>(); }

        public Int64 id { get; set; }
        public string name { get; set; }
        public string workspace { get; set; }
        public string created { get; set; }
        public string modified { get; set; }
        public Int64 creatorId { get; set; }
        public Int64 ownerId { get; set; }
        public List<Collaborator> collaborators { get; set; }
        public int loadCount;
        public int attachmentCount;
    }
    
    public class User
    {
        public Int64 userId { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string firstShared { get; set; }
    }

    public class JsonData
    {
        public JsonData() { sheets = new List<Sheet>(); users = new List<User>(); }

        public List<Sheet> sheets { get; set; }
        public List<User> users { get; set; }
    }

    public class Attachment
    {
        public Int64 id { get; set; }
        public string name { get; set; }
        public string attachmentType { get; set; }
        public string mimeType { get; set; }
        public int sizeInKb { get; set; }
        public string parentType { get; set; }
        public Int64 parentId { get; set; }
        public string createdAt { get; set; }
    }

    public class AttachmentData
    {
        public AttachmentData() { attachments = new List<Attachment>(); }
        public List<Attachment> attachments { get; set; } 
    }


    const string TEST_CLIENT_ID = "pvavuyt47jqqfvmpyp";
    const string TEST_CLIENT_SECRET = "ztohl6htj835mxk7kg";

    const string PRODUCTION_CLIENT_ID = "r96hkslse1743jhjf1";
    const string PRODUCTION_CLIENT_SECRET = "1sedhjl1juog508ik7j";

    const string TEST_PROTOCOL = "http";
    const string TEST_HOST_NAME = "synapticswitch.com/advaiya";

    const string REDIRECT_PAGE = "/Validate.aspx";

    private string strClientId = "";
    private string strClientSecret = "";
    private string strRequestCode = "";
    private string strAccessToken = "";

    private string strRedirectUrl = "";
    private string strRedirectPage = "/Validate.aspx";
    private Uri pTargetUrl = new Uri("https://www.smartsheet.com/");

    private UserData pUserData = new UserData();
    private JsonData pJsonData = new JsonData();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Uri pLocation = Request.Url;
        var strProtocol = pLocation.Scheme;
        var strHostName = pLocation.Host;
        var strRedirectUrl = "";

        if (!Page.ClientScript.IsStartupScriptRegistered("Render"))
        {

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Render", "Render();", true);

        }
        if (DEBUG_LOCAL == true)
        {
            strRedirectUrl = TEST_PROTOCOL + "://" + TEST_HOST_NAME + REDIRECT_PAGE;
            strClientId = TEST_CLIENT_ID;
            strClientSecret = TEST_CLIENT_SECRET;
        }
        else
        {
            strRedirectUrl = strProtocol + "://" + strHostName + strRedirectPage;
            strClientId = PRODUCTION_CLIENT_ID;
            strClientSecret = PRODUCTION_CLIENT_SECRET;
        }

        try
        {
            //Response.Write("<h1 style='z-index:9999; color:black;'>TRY</h1>");
            if (Session["request_code"] != null)
            {
                //Response.Write("<h1 style='z-index:9999; color:black;'>1</h1>");
                strRequestCode = Session["request_code"].ToString();
                strAccessToken = Session["access_token"].ToString();

                GetUserInfo(strAccessToken);
                //Response.Write("<h1 style='z-index:9999; color:black;'>6</h1>");

                //Save data in session 
                Session["access_token"] = strAccessToken;
                Session["user_id"] = pUserData.id.ToString();
                Session["email"] = pUserData.email;
                Session["request_code"] = strRequestCode;

                //Save data in hidden data fields to allow JavaScript
                userEmail.Value = pUserData.email;
                userId.Value = pUserData.id.ToString();
                //Response.Write("<h1 style='z-index:9999; color:black;'> DATA: " + userId.Value + "</h1>");
                accessToken.Value = strAccessToken;

                GetPackage(strAccessToken);

                for(int i = 0; i < pJsonData.sheets.Count; i++)
                {
                    pJsonData.sheets[i].attachmentCount = GetNumberOfAttachments(strAccessToken, pJsonData.sheets[i].id);
                }

                var json = new JavaScriptSerializer().Serialize(pJsonData);

                jsonData.Value = json.ToString();

                if(jsonData.Value.Length == 0)
                {
                    Response.Write("<h1 style='z-index:9999; color:black;'>No Data</h1>");
                    Response.Redirect("NotAllowed.html");
                }
            }
            else
            {
                //Response.Write("<h1 style='z-index:9999; color:black;'>2</h1>");
                if (Page.Request.QueryString["code"] != null)
                {
                    //Response.Write("<h1 style='z-index:9999; color:black;'>3</h1>");
                    strRequestCode = Page.Request.QueryString["code"];
                    if (strRequestCode != null)
                    {
                        //Response.Write("<h1 style='z-index:9999; color:black;'>4</h1>");
                        strAccessToken = GetAccessToken(strRequestCode);
                        if (strAccessToken != null)
                        {
                            //Response.Write("<h1 style='z-index:9999; color:black;'>5</h1>");
                            GetUserInfo(strAccessToken);
                            //Response.Write("<h1 style='z-index:9999; color:black;'>6</h1>");

                            //Save data in session 
                            Session["access_token"] = strAccessToken;
                            Session["user_id"] = pUserData.id.ToString();
                            Session["email"] = pUserData.email;
                            Session["request_code"] = strRequestCode;

                            //Save data in hidden data fields to allow JavaScript
                            userEmail.Value = pUserData.email;
                            userId.Value = pUserData.id.ToString();
                            //Response.Write("<h1 style='z-index:9999; color:black;'> DATA: "+ userId.Value + "</h1>");
                            accessToken.Value = strAccessToken;
                            GetPackage(strAccessToken);
                            for (int i = 0; i < pJsonData.sheets.Count; i++)
                            {
                                pJsonData.sheets[i].attachmentCount = GetNumberOfAttachments(strAccessToken, pJsonData.sheets[i].id);
                            }

                            var json = new JavaScriptSerializer().Serialize(pJsonData);

                            jsonData.Value = json.ToString();

                            if (jsonData.Value.Length == 0)
                            {
                                Response.Write("<h1 style='z-index:9999; color:black;'>No Data</h1>");
                                Response.Redirect("NotAllowed.html");
                            }
                        }
                        else
                        {
                            Debug.Write("No Access Token");
                            Response.Redirect("NotAllowed.html");
                        }
                    }
                    else
                    {
                        Debug.Write("No Request Code");
                        Response.Redirect("NotAllowed.html");
                    }
                }
                else
                {
                    Debug.Write("No Access Code");
                    Response.Redirect("NotAllowed.html");
                }
            }
        }
        catch (WebException ex)
        {
            Debug.Write(ex.Message);
            Response.Redirect("NotAllowed.html");
        }
    }
    private void GetUserInfo(string p_strAccessToken)
    {
        var strUrl = "https://api.smartsheet.com/1.1/user/me";

        var pRequest = HttpWebRequest.Create(strUrl);
        pRequest.Method = "GET";

        pRequest.ContentType = "application/x-www-form-urlencoded";
        pRequest.Headers.Add("Authorization: Bearer " + p_strAccessToken);

        using (WebResponse response = pRequest.GetResponse())
        {
            var dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);
            var serializer = new JavaScriptSerializer();
            pUserData = serializer.Deserialize<UserData>(reader.ReadToEnd());
            reader.Close();
            dataStream.Close();
        }
    }

    private void GetPackage(string p_strAccessToken)
    {
        string strUrl = "https://api.smartsheet.com/1.1/marketing/_mystory1";

        var pRequest = HttpWebRequest.Create(strUrl);
        pRequest.Method = "GET";

        pRequest.ContentType = "application/x-www-form-urlencoded";
        pRequest.Headers.Add("Authorization: Bearer " + p_strAccessToken);

        string result;

        using (WebResponse response = pRequest.GetResponse())
        {
            Stream dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);

            result = reader.ReadToEnd();

            //Save the data to a local hidden field.
            //jsonData.Value = result;
            reader.Close();
            dataStream.Close();
        }

        
        byte[] byteArray = Encoding.ASCII.GetBytes(result);
        MemoryStream jsonStream = new MemoryStream( byteArray );
 
        var jsonReader = new StreamReader(jsonStream);
        var serializer = new JavaScriptSerializer();
        pJsonData = serializer.Deserialize<JsonData>(jsonReader.ReadToEnd());

    }

    private string GetAccessToken(string p_strRequestToken)
    {
        string strUrl = "https://api.smartsheet.com/1.1/token";

        var pRequest = HttpWebRequest.Create(strUrl);
        pRequest.Method = "POST";

        var postDataString = new StringBuilder();
        postDataString.Append("grant_type=authorization_code");
        postDataString.Append("&client_id=" + strClientId);

        string secretHash = strClientSecret + "|" + p_strRequestToken;
        byte[] data = System.Text.Encoding.UTF8.GetBytes(secretHash);
        byte[] hash = SHA256.Create().ComputeHash(data);

        string hexvalue = string.Concat(hash.Select(b => b.ToString("X2")));

        postDataString.Append("&hash=" + Server.UrlEncode(hexvalue));
        postDataString.Append("&code=" + Server.UrlEncode(p_strRequestToken));
        postDataString.Append("&redirect_uri=" + Server.UrlEncode(strRedirectUrl));

        var postDataArray = Encoding.UTF8.GetBytes(postDataString.ToString());
        pRequest.ContentType = "application/x-www-form-urlencoded";
        pRequest.ContentLength = postDataArray.Length;

        var dataStream = pRequest.GetRequestStream();
        dataStream.Write(postDataArray, 0, postDataArray.Length);
        dataStream.Close();

        var pAcsData = new ACSData();
        using (WebResponse response = pRequest.GetResponse())
        {
            dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);
            var serializer = new JavaScriptSerializer();
            pAcsData = serializer.Deserialize<ACSData>(reader.ReadToEnd());
            reader.Close();
            dataStream.Close();
        }
        return pAcsData.access_token;
    }

    private int GetNumberOfAttachments(string p_strAccessToken, Int64 p_iDocumentId)
    {
        //GET /sheet/{sheetId}/attachments
        
        string strUrl = "https://api.smartsheet.com/1.1/sheet/";
        strUrl += p_iDocumentId.ToString() + "/attachments";

        var pRequest = HttpWebRequest.Create(strUrl);
        pRequest.Method = "GET";

        pRequest.ContentType = "application/x-www-form-urlencoded";
        pRequest.Headers.Add("Authorization: Bearer " + p_strAccessToken);

        string result = "";

        using (WebResponse response = pRequest.GetResponse())
        {
            Stream dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);

           result = reader.ReadToEnd();

            reader.Close();
            dataStream.Close();
        }
        string pattern = "\"id\"";

        int iNumberOfAttachments = CountStringOccurrences(result, pattern);

        return iNumberOfAttachments;

    }

    public int CountStringOccurrences(string text, string pattern)
    {
        // Loop through all instances of the string 'text'.
        int count = 0;
        int i = 0;
        while ((i = text.IndexOf(pattern, i)) != -1)
        {
            i += pattern.Length;
            count++;
        }

        return count;
    }


    void GenerateEmail(string p_strFromEmail, string p_strToEmail, string p_strSubject, string p_strBodyMessage, string p_strImageName)
    {
        System.Net.Mail.MailMessage pMessage = new System.Net.Mail.MailMessage();
        pMessage.From = new System.Net.Mail.MailAddress(p_strFromEmail);
        pMessage.To.Add(p_strToEmail);
        pMessage.Subject = p_strSubject;

        System.Net.Mail.AlternateView pViewPlainText = System.Net.Mail.AlternateView.CreateAlternateViewFromString(p_strBodyMessage, null, "text/plain");

        System.Net.Mail.AlternateView pViewHtml = System.Net.Mail.AlternateView.CreateAlternateViewFromString(p_strBodyMessage + "<img src=cid:HDImage />", null, "text/html");

        System.Net.Mail.LinkedResource pResource = new System.Net.Mail.LinkedResource(p_strImageName);
        pResource.ContentId = "HDImage";

        pViewHtml.LinkedResources.Add(pResource);
        pMessage.AlternateViews.Add(pViewHtml);
        pMessage.AlternateViews.Add(pViewPlainText);

        System.Net.Mail.SmtpClient pSmtpClient = new System.Net.Mail.SmtpClient("mail.google.com");
        pSmtpClient.Port = 25;
        pSmtpClient.Credentials = new NetworkCredential("synapticswitch", "Madc0d3r");
        pSmtpClient.EnableSsl = true;

        pSmtpClient.Send(pMessage);

    }
    protected void email_button_Click(object sender, ImageClickEventArgs e)
    {
        GenerateEmail("rmadsen@synapticswitch.com", "rmadsen@iname.com", "Test", "Test", "resources/images/email_button_wide.png");

    }


}
