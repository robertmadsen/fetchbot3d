﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Validate.aspx.cs" Inherits="Validate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Your WorkMap</title>
    <link rel="stylesheet" type="text/css" href="resources/css/Interface.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/WorkMap.css" />

    <script src="resources/js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="resources/js/popup.js" type="text/javascript"></script>
    <script src="resources/js/three.min.js" type="text/javascript"></script>
    <script src="resources/js/Auth.js" type="text/javascript"></script>
    <script src="resources/js/TrackballControls.js" type="text/javascript"></script>
    <script src="resources/js/Configuration.js" type="text/javascript"></script>
    <script src="resources/js/game.js" type="text/javascript"></script>
    <script src="resources/js/Data.js" type="text/javascript"></script>
    <script src="resources/js/Stats.js" type="text/javascript"></script>
    <script src="resources/js/SelectedSheet.js" type="text/javascript"></script>
    <script src="resources/js/html2canvas.js" type="text/javascript"></script>
    <script src="resources/fonts/helvetiker_regular.typeface.js"></script>
    <script src="resources/js/Interface.js" type="text/javascript"></script>

    <script type="text/javascript">
        function Render()
        {
            console.log("Render begin");
            var pUserEmail = document.getElementById("userEmail");
            var pUserId = document.getElementById("userId");
            var pAccessToken = document.getElementById("accessToken");

            var strUserEmail = pUserEmail.value;
            var strUserId =  pUserId.value;
            var strAccessToken = pAccessToken.value;

            console.log("User id (Render function) " + strUserId);

            sessionStorage["fetchBot3d.userEmail"] = strUserEmail;
            sessionStorage["fetchBot3d.accessToken"] = strAccessToken;
            sessionStorage["debug.dataset"] = strUserId;
            sessionStorage["fetchBot3d.authorized"] = "true";

            initScreen('3D');
            initHeader();
            initConfiguration();
        }
    </script>
</head>
<body>
    <form id="frmServer" runat="server">

    <asp:HiddenField ID="jsonData" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="userEmail" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="userId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="accessToken" runat="server" ClientIDMode="Static" />

    <div class="workmap_window">
        <div class="workmap_page">
            <div class="workmap_header_block">
                <img id="workmap_header_block_background" class="workmap_header_block_background" src="resources/images/background_no_image.png" alt="Background" />
                <img id="logo" class="logo" src="resources/images/smartsheetlogo.png" alt="Smartsheet" />
                <div id="user_email" class="user_email">info@smartsheet.com</div>
                <div id="options_arrow" class="options_arrow" onmousedown="SwitchOptions();">
                    <img id="options_arrow_image" src="resources/images/options_arrow_right.png" alt="Options" />
                </div>
                <div id="logout" class="logout" onmousedown="logout();">Logout</div>
                <div id="help_button" class="help_button">
                    <a href="http://smartsheet.com/workmap-overview" target="_blank">
                        <img src="resources/images/help_button.png" alt="Help" />
                    </a>
                </div>
            </div>
            <div id="stat_panel_block" class="stat_panel_block" onmouseout="setCameraControls(false);" onmouseover="    setCameraControls(true);">
                <img id="stat_panel_wide" class="stat_panel_wide" src="resources/images/stats_panel_wide.png" alt="Statistics" />
                <img id="stat_panel_narrow" class="stat_panel_narrow" src="resources/images/stats_panel_narrow.png" alt="Statistics" />
                <div id="stat_text_block" class="stat_text_block">
                    <div id="stat_total_sheets" class="stat_total_sheets">&nbsp;</div>
                    <div id="stat_my_sheets" class="stat_my_sheets">1</div>
                    <div id="stat_other_sheets" class="stat_other_sheets">&nbsp;</div>
                    <div id="stat_total_connections" class="stat_total_connections">&nbsp;</div>
                    <div id="stat_direct_connections" class="stat_direct_connections">&nbsp;</div>
                    <div id="stat_indirect_connections" class="stat_indirect_connections">&nbsp;</div>

                    <div id="stat_selected_sheets" class="stat_selected_sheets">&nbsp;</div>
                    <div id="stat_selected_connections" class="stat_selected_connections">&nbsp;</div>
                    <div id="stat_selected_attachments" class="stat_selected_attachments">&nbsp;</div>

                    <div id="billboard_text_block" class="billboard_text_block">
                        <div id="billboard_item" class="billboard_item"></div>
                        <div id="billboard_date" class="billboard_date"></div>
                        <div id="billboard_name" class="billboard_name"></div>
                    </div>

                    <a onclick="GenerateDownload();">
                        <img id="download_button_wide" class="download_button_wide" src="resources/images/download_button_wide.png" alt="Download" />
                    </a>

                </div>
            </div>

            <div id="carousel_block" class="carousel_block">
                <img src="resources/images/building_your_work_map_screen.png" alt="Building Your Workmap" />
            </div>
        </div>
        <div id="overlay_block" class="overlay_block">
            <img id="zoom_indicator" class="zoom_indicator" src="resources/images/zoom_gauge.png" alt="zoom meter" />
            <img id="zoom_selector" class="zoom_selector" src="resources/images/zoom_selection.png" alt="zoom level" />
        </div>
    </div>
</form>
</body>
</html>
