﻿{
	"maxSheets": 600,
	"maxPeople": 1000,
	"numberOfFavorites":3,
	"sizePersonNormal":40,
	"platformWidthMin":2000,
	"platformLengthMin":3000,
	"platformHeight":50,
	"platformShadowSeparation":250,
	"personLargeHeightMin":40,
	"personLargeHeightMax":60,
	"personSmallHeightMin":20,
	"personSmallHeightMax":30,
	"sheetWidth":90,
	"sheetLength":90,
	"sheetNormalHeight":10,
	"sheetSelectedHeight":10,
	"sheetSeparationMin":10,
	"sheetSeparationMax":15,
  "numberOfStars":100,

  "captionOffset":{"x":0, "y":10, "z":0},
  
  "cameraOffset":{"x":0, "y":100, "z":-2000},
  
  "enableCameraControls":false,
  "enableKeyboardControls":true
}