﻿/// <reference path="Configuration.js" />

//Change this to false for production deployment
var DEBUG_LOCAL = true;

var TEST_CLIENT_ID = "pvavuyt47jqqfvmpyp";
var TEST_CLIENT_SECRET = "ztohl6htj835mxk7kg";

var PRODUCTION_CLIENT_ID = "r96hkslse1743jhjf1";
var PRODUCTION_CLIENT_SECRET = "1sedhjl1juog508ik7j";

var TEST_PROTOCOL = "http:";
var TEST_HOST_NAME = "synapticswitch.com/advaiya";

var REDIRECT_PAGE = "/Validate.aspx";

var strClientId = "";
var strClientSecret = "";
var strTargetUrl = "https://www.smartsheet.com/";
var strScope = "READ_SHEETS";
var strRedirectUrl;

function login() {
    var pLocation = parseLocation(window.location);
    var strProtocol = pLocation.protocol;
    var strHostName = pLocation.hostname;
    var strHostPath = pLocation.pathname;
    var strRedirectPage = "/Validate.aspx";

    if (webgl_support() == false) window.location = "NotSupported.html";

    if (DEBUG_LOCAL == true)
    {
        strRedirectUrl = TEST_PROTOCOL + "//" + TEST_HOST_NAME + REDIRECT_PAGE;
        strClientId = TEST_CLIENT_ID;
        strClientSecret = TEST_CLIENT_SECRET;
    }
    else
    {
        strRedirectUrl = strProtocol + "//" + strHostName + strRedirectPage;
        strClientId = PRODUCTION_CLIENT_ID;
        strClientSecret = PRODUCTION_CLIENT_SECRET;
    }

    var strLoginUrl = strTargetUrl + "b/authorize?client_id=" + strClientId + "&client_secret=" + strClientSecret + "&scope=" + strScope + "&response_type=code&redirect_uri=" + strRedirectUrl;
    console.log("Validating: " + strLoginUrl);
    window.setTimeout(function ()
    {
        window.location = strLoginUrl;
    }, 10);
}

function webgl_support()
{
    try
    {
        var canvas = document.createElement('canvas');
        return !!window.WebGLRenderingContext && (
             canvas.getContext('webgl') || canvas.getContext('experimental-webgl'));
    } catch (e) { return false; }
};


function validate()
{
    var bAuthorized = false;
    var strAccessToken = getQueryStringParameter("accessToken");
    //console.log("Validating Request")
    if (strAccessToken != null)
    {
        bAuthorized = true;
    }
    
    var strEmail = getQueryStringParameter("email");

    var intId = getQueryStringParameter("userid");

    if (bAuthorized == true)
    {
        sessionStorage["fetchBot3d.authorized"] = "true";
        sessionStorage["fetchBot3d.accessToken"] = strAccessToken;
        sessionStorage["fetchBot3d.userEmail"] = strEmail;
        sessionStorage["debug.dataset"] = intId;


        showMap();
    }
    else
    {
        window.location = "NotAllowed.html";
    }
}


function getQueryStringParameter(key, queryString) {

    var queryString = queryString || window.location.href;
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(queryString);
    if (qs)
        return qs[1];
    else
        return null;
}


function parseLocation(href)
{
    var l = document.createElement("a");
    l.href = href;
    return l;
}

function makeRequest(p_strAccessToken, p_strUrl)
{
    //console.log("MakeRequest: " + p_strUrl + " " + p_strAccessToken);
    $.ajax({

        // The 'type' property sets the HTTP method.
        // A value of 'PUT' or 'DELETE' will trigger a preflight request.
        type: 'GET',
        crossDomain: true,
        dataType: 'jsonp',

        // The URL to make the request to.
        url: p_strUrl,

        // The 'contentType' property sets the 'Content-Type' header.
        // The JQuery default for this property is
        // 'application/x-www-form-urlencoded; charset=UTF-8', which does not trigger
        // a preflight. If you set this value to anything other than
        // application/x-www-form-urlencoded, multipart/form-data, or text/plain,
        // you will trigger a preflight request.
        contentType: 'application/json',

        xhrFields: {
            // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
            // This can be used to set the 'withCredentials' property.
            // Set the value to 'true' if you'd like to pass cookies to the server.
            // If this is enabled, your server must respond with the header
            // 'Access-Control-Allow-Credentials: true'.
            withCredentials: false
        },

        headers: {
            Authorization: 'BEARER ' + p_strAccessToken,
        },

        success: function (data)
        {
            // Here's where you handle a successful response.
            //console.log("Success: " + data.toString());
        },

        error: function (error)
        {
            // Here's where you handle an error response.
            // Note that if the error was due to a CORS issue,
            // this function will still fire, but there won't be any additional
            // information about the error.
            //console.log("Error: " + error.toString());
        }
    });
}
