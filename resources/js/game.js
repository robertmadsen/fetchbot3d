﻿//GLOBAL DECLARATIONS

var DEBUG_USE_SMALL_DATASET = 0;
var DEBUG_USE_LARGE_DATASET = 1;
var DEBUG_USE_LOCAL_DATA = true;

//Stats
var iPlayerScore = 0;
var iPlayerLevel = 0;
var bGameOver = false;

//State
var bUpdatedOnce = false;
var bEventInProgress = false;

//Player
var pPlayer = null;

//Gameobjects
var arrGameObjects = Array();

function startGame(data, configuration) {

  var config = getConfigData();

  if (DEBUG_USE_LOCAL_DATA == true) {
    if (DEBUG_USE_LARGE_DATASET == true) {
      sessionStorage["debug.dataset"] = DEBUG_USE_LARGE_DATASET;
    }
    else {
      sessionStorage["debug.dataset"] = DEBUG_USE_SMALL_DATASET;
    }
  }


  //Render objects and variables
  var camera = null;
  var controls = null;
  var scene = null;
  var projector = null;
  var renderer = null;

  var targetRotation = 0;
  var targetRotationOnMouseDown = 0;

  var windowHalfX = window.innerWidth / 2;
  var windowHalfY = window.innerHeight / 2;

  //Input objects and variables
  var mouse = null;
  var mouseX = 0;
  var mouseXOnMouseDown = 0;
  var mouseY = 0;
  var mouseYOnMouseDown = 0;
  var isMouseDown = false;

  //Data objects and variables
  var people = data.people;
  var sheets = data.sheets;
  var userLoggedIn = eval(sessionStorage["debug.dataset"]);

  console.log("USER ID " + userLoggedIn);
  var iNumberOfPeopleX;
  var iNumberOfPeopleZ;

  //OBJECTS

  //Create camera
  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 100000);

  //Create renderer
  var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setClearColor(0x000000, 1);
  renderer.shadowMapEnabled = true;
  renderer.shadowMapType = THREE.PCFSoftShadow;
  renderer.shadowMapSoft = true;
  renderer.shadowCameraNear = 1000;
  renderer.shadowCameraFar = 10000;
  renderer.shadowCameraFov = 50;
  renderer.shadowMapBias = 0.0039;
  renderer.shadowMapDarkness = 0.25;
  renderer.shadowMapWidth = 10000;
  renderer.shadowMapHeight = 10000;
  renderer.physicallyBasedShading = true;
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  //Create scene
  var scene = new THREE.Scene();

  //INPUT
  var bEnableCameraControls = config.enableCameraControls;
  var bEnableKeyboardControls = config.enableKeyboardControls;


  //Setup mouse
  mouse = new THREE.Vector2();

  //Create trackball controls
  controls = new THREE.TrackballControls(camera);
  controls.rotateSpeed = 1.0;
  controls.zoomSpeed = 1.2;
  controls.panSpeed = 0.8;
  controls.noZoom = false;
  controls.noPan = true;
  controls.staticMoving = true;
  controls.dynamicDampingFactor = 0.3;

  //Create scene lighting
  scene.add(new THREE.AmbientLight(0x333333));

  var directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);

  directionalLight.position.set(0, 1, 0);
  directionalLight.castShadow = true;
  directionalLight.shadowDarkness = 0.35;
  directionalLight.shadowCameraFov = 60;
  directionalLight.shadowCameraLeft = -3000;
  directionalLight.shadowCameraRight = 3000;
  directionalLight.shadowCameraTop = 3500;
  directionalLight.shadowCameraBottom = -3000;
  directionalLight.shadowMapWidth = 2048;
  directionalLight.shadowMapHeight = 2048;


  scene.add(directionalLight);

  //Create rendered object groups
  var masterGroup = new THREE.Object3D();
  var peopleGroup = new THREE.Object3D();
  var sheetGroup = new THREE.Object3D();
  var lineGroup = new THREE.Object3D();
  var nameTextGroup = new THREE.Object3D();

  masterGroup.castShadow = true;

  //Create planes

  var iPeoplePlaneWidth = 400;
  var peoplePlaneHeight = PEOPLE_PLANE_THICKNESS;
  var iPeoplePlaneLength = 400;
  //if (iPeoplePlaneWidth < 2000) iPeoplePlaneWidth = 2000;
  //if (iPeoplePlaneLength < 3000) iPeoplePlaneLength = 3000;
  var peoplePlane = new THREE.Mesh(new THREE.CubeGeometry(iPeoplePlaneWidth, peoplePlaneHeight, iPeoplePlaneLength), new THREE.MeshLambertMaterial({
    color: 0xcccccc
  }));

  var iRightEdge = iPeoplePlaneWidth;
  var iLeftEdge = 0;
  var iTopEdge = 0;
  var iBottomEdge = iPeoplePlaneLength;

  peoplePlane.castShadow = true;
  peoplePlane.receiveShadow = true;

  var baseWidth = 20000;
  var baseLength = baseWidth;
  var baseHeight = 1;

  var basePlane = new THREE.Mesh(new THREE.CubeGeometry(baseWidth, baseHeight, baseLength), new THREE.MeshLambertMaterial({
    color: 0xA0A0A0
  }));
  basePlane.receiveShadow = true;

  //Calcuate number of shares per person
  accumulateShares(sheets, people);

  //Calcuate sheet plane size
  var iNumberOfSheets = data.sheets.length;
  iTotalSheets = iNumberOfSheets; //Stat
  var iMaxSheetX = Math.ceil(Math.sqrt(iNumberOfSheets));
  var iMaxSheetZ = iMaxSheetX * 1.5; //this sets a 2:3 aspect ratio
  var iNumberOfCells = iMaxSheetX * iMaxSheetZ;
  var iSheetSize = 90;    //Per redline specs
  var iSheetGap = 10;     //Per redline specs
  var iSheetSpacingFactorX = iSheetSize + iSheetGap;
  var iSheetSpacingFactorZ = iSheetSize + iSheetGap;
  var iSheetFixedY = 45;   //We just like this number!

  var iSheetsPlaneWidth = (iSheetSpacingFactorX * iMaxSheetX) - iSheetGap;
  var iSheetsPlaneLength = (iSheetSpacingFactorZ * iMaxSheetZ) - iSheetGap;

  var sheetsPlane = null;

  iTotalConnections = data.people.length - 1;     //Stat...-1 becuase connections is 1 less than the total number of people

  peoplePlane.position.x = 0;
  peoplePlane.position.y = -500;
  peoplePlane.position.z = 0.0;

  peopleGroup.position.x = peoplePlane.position.x - (iPeoplePlaneWidth / 2.0);
  peopleGroup.position.y = peoplePlane.position.y;
  peopleGroup.position.z = peoplePlane.position.z - (iPeoplePlaneLength / 2.0);


  //Arrow Object Creation
  var pMeshRightArrow;
  var pMeshLeftArrow;
  var pMeshBottomArrow;


  peoplePlane.position.x = 0;
  peoplePlane.position.y = -500;
  peoplePlane.position.z = 0.0;

  createArrows();


  //console.log("Max sheets x/z" + iMaxSheetX + " / " + iMaxSheetZ);
  //console.log("Sheets plane width/length " + iSheetsPlaneWidth + " / " + iSheetsPlaneLength);

  basePlane.position.x = 0;
  basePlane.position.y = BASE_PLANE_PEOPLE_PLANE_SEPARATION;
  basePlane.position.z = 0;

  masterGroup.position.x = 0;
  masterGroup.position.y = 0;
  masterGroup.position.z = 0;

  masterGroup.add(basePlane);
  masterGroup.add(peoplePlane);

  masterGroup.add(pMeshRightArrow);
  masterGroup.add(pMeshLeftArrow);
  masterGroup.add(pMeshBottomArrow);

  masterGroup.add(nameTextGroup);
  masterGroup.add(peopleGroup);
  masterGroup.add(sheetGroup);
  if (DEBUG_DRAW_SHEETS_PLANE == true) {
    masterGroup.add(sheetsPlane);
  }

  lineGroup.position.x = peopleGroup.position.x;
  lineGroup.position.y = peopleGroup.position.y;
  lineGroup.position.z = peopleGroup.position.z;
  masterGroup.add(lineGroup);

  scene.add(masterGroup);

  //Set final camera position adjusted for size of sheets/people plane
  var iPlaneDepth = iSheetsPlaneLength > iPeoplePlaneLength ? iSheetsPlaneLength : iPeoplePlaneLength;
  var iPlaneWidth = iSheetsPlaneWidth > iPeoplePlaneWidth ? iSheetsPlaneWidth : iPeoplePlaneWidth;
  var iPlaneHeight = PEOPLE_PLANE_SHEET_PLANE_SEPARATION - BASE_PLANE_PEOPLE_PLANE_SEPARATION + PEOPLE_PLANE_THICKNESS;


  camera.position.x = config.cameraOffset.x;
  camera.position.y = config.cameraOffset.y;
  camera.position.z = config.cameraOffset.z;

  var vMyPersonPosition = peoplePlane.position;

  var pUserLoggedIn = null;
  var pMyPersonName = null;
  var pPersonHoveringMesh = null;
  var pPersonHoveringMeshMat = null;
  var arrSheetsHovering = new Array();
  var arrSheetsHoveringMats = new Array();
  var arrSheetsSelected = new Array();

  var pYellowLineMat = new THREE.LineBasicMaterial({
    color: 0xd5a728
  });


  var pGreyLineMat = new THREE.LineBasicMaterial({
    color: 0xd5d9e1
  });

  var pYellowSheetMat = new THREE.MeshLambertMaterial({
    color: 0xd5a728
  });

  var pGreenSheetMat = new THREE.MeshLambertMaterial({
    color: 0x067606
  });

  var pBlueSheetMat = new THREE.MeshLambertMaterial({
    color: 0x518fb4
  });

  var pWhiteSheetMat = new THREE.MeshLambertMaterial({
    color: 0xFFFFFF
  });

  var texture = THREE.ImageUtils.loadTexture("resources/images/person_gray.png");
  var pGreenPersonMat = new THREE.MeshBasicMaterial({ transparent: true, map: texture, overdraw: true, color: 0x067606 });

  //Assign the favorites of this user's sheets
  assignFavoritesToUser(userLoggedIn, sheets, config.numberOfFavorites);

  //Draw directly connected people and network, with earliest first shared being drawn first
  var arrDirectUsers = extractUniqueSheetOwners(userLoggedIn, sheets, people);
  if (arrDirectUsers.length > 1) {
    arrDirectUsers.sort(function (a, b) {
      a = new Date(a.firstShared);
      b = new Date(b.firstShared);
      return a > b ? 1 : a < b ? -1 : 0;
    });
  }

  var earliestYear = parseInt(arrDirectUsers[0].firstShared.substring(0, 4));
  //console.log("Earliest: ", earliestYear);

  var numberOfDirectUsers = (arrDirectUsers.length);
  populatePeople(arrDirectUsers, vMyPersonPosition, true);
  drawGrayLines(arrDirectUsers, pUserLoggedIn, true);

  //Draw indirectly connected people and network with the most recent being drawn first
  var arrIndirectUsers = extractUniqueCollaborators(userLoggedIn, sheets, people);
  //First, sort ascending by date to find earliest year
  if (arrIndirectUsers.length > 0) {
    if (arrIndirectUsers.length > 1) {
      arrIndirectUsers.sort(function (a, b) {
        a = new Date(a.firstShared);
        b = new Date(b.firstShared);
        return a > b ? 1 : a < b ? -1 : 0;
      });
    }
    var checkYear = parseInt(arrIndirectUsers[0].firstShared.substring(0, 4));
    if (checkYear < earliestYear) earliestYear = checkYear;
  }


  //Plot the indirect users
  if (arrDirectUsers.length > 1) {
    //Now sort descending by date to plot the most recent first
    arrDirectUsers.sort(function (a, b) {
      a = new Date(a.firstShared);
      b = new Date(b.firstShared);
      return a > b ? -1 : a < b ? 1 : 0;
    });
  }
  for (var i = 0; i < arrDirectUsers.length; i++) {
    var arrCurrentGroup = new Array();
    var iCount = 0;
    var pPrimary = arrDirectUsers[i];
    //Find sheets owned by this user
    for (var j = 0; j < sheets.length; j++) {
      pSheet = sheets[j];
      if (pSheet.ownerId == pPrimary.userId) {

        //Find the collaborators on this sheet
        for (var k = 0; k < pSheet.collaborators.length; k++) {
          pCollaborator = pSheet.collaborators[k];

          //Find this collaborator in our list of indirect users
          for (l = 0; l < arrIndirectUsers.length; l++) {
            pIndirect = arrIndirectUsers[l];
            if (pIndirect.userId == pCollaborator.userId) {
              //If this person hasn't already been plotted
              if (pIndirect.attached == null || pIndirect.attached == false) {
                pIndirect.attached = true;
                arrCurrentGroup[iCount] = pIndirect;
                iCount++;
                //iIndirectConnections++;
              }
              break;
            }
          }
        }
      }

    }
    //Now plot these collaborators
    if (arrCurrentGroup.length > 0) {
      populatePeople(arrCurrentGroup, pPrimary.position, false);
      //drawGrayLines(arrCurrentGroup, pPrimary, false);
    }
  }

  //for (var i = 0; i < peopleGroup.children.length; i++)
  //{
  //    console.log("PI: " + peopleGroup.children[i].userData.index);
  //}

  populateSheets();

  //Preselect the three favorite sheets
  for (var i = 0; i < sheets.length; i++) {
    if (sheets[i].favorite != null && sheets[i].favorite == true) {
      sheetSelected(sheetGroup.children[i]);
    }
  }

  //Timeline deprecated for now
  //drawTimeline();

  //Hover Text 
  var pObjectHoveringText = null;

  //Event handlers
  document.addEventListener('mousemove', onDocumentMouseMove, false);
  document.addEventListener('mousedown', onDocumentMouseDown, false);
  document.addEventListener('touchstart', onDocumentTouchStart, false);
  document.addEventListener('touchmove', onDocumentTouchMove, false);

  window.addEventListener('resize', onWindowResize, false);
  window.addEventListener("keypress", onKeyPress, false)

  //EVENT HANDLERS

  var projector = new THREE.Projector();
  var mouseVector = new THREE.Vector3();

  function initPlayer() {
    var vPosition = { x: 0, y: 0, z: 0 };
    pPlayerGroup = createPerson(pPlayer, p_bDirect);

    vPosition.x = i + iPeoplePlaneWidth / 2;
    vPosition.y = iPersonFixedY + (pPerson.height / 2);
    vPosition.z = j + iPeoplePlaneLength / 2;

    pPlayerGroup.position = vPosition;


  }

  function updatePlayer()
  {

    var vPosition = { x: 0, y: 0, z: 0 };

    vPosition.x = i + iPeoplePlaneWidth / 2;
    vPosition.y = iPersonFixedY + (pPerson.height / 2);
    vPosition.z = j + iPeoplePlaneLength / 2;

    pPlayerGroup.position = vPosition;

    pPersonGroup.position = vMyPersonPosition;

    //Save position and object to sheet data object
    var vPosition = { x: x, y: y, z: z };
    pPerson.position = vPosition;

    var playerName = "Robert";
    if (playerName == null || playerName == "")
      playerName = "?";

    var text = "";
    var textMesh = null;

    if (text != "") {
      var textReturn = createText(text, true, 30);
      var textObject = textReturn.obj;
      var textObjectWidth = textReturn.width;

      textObject.position.x = config.captionOffset.x;
      textObject.position.y = config.captionOffset.y;
      textObject.position.z = config.captionOffset.z;
      pPersonGroup.add(textObject);
    }

    //vMyPersonPosition = pPerson.position;
    pUserLoggedIn = pPerson;
    pMyPersonName = textObject;

  }

  function populatePeople(p_arrPeople, p_vSourcePosition, p_bDirect) {
    var iNumberOfPeople = p_arrPeople.length;
    var iMaxPeopleX = Math.ceil(Math.sqrt(iNumberOfPeople));
    var iMaxPeopleZ = iMaxPeopleX;

    //If this is being done for direct users, save the value globally for future use
    if (p_bDirect == true) {
      iNumberOfPeopleX = iMaxPeopleX;
      iNumberOfPeopleZ = iMaxPeopleZ;
    }

    var iPlaneBorderX = 200;
    var iPlaneBorderZ = 300;
    var iPlotWidth;
    var iPlotLength;
    var iXOffset;
    var iZOffset;


    if (p_bDirect == true) {
      iPlotWidth = iPeoplePlaneWidth - (iPlaneBorderX * 2);
      iXOffset = iPlaneBorderX * 3;
      iPlotLength = iPeoplePlaneLength - (iPlaneBorderZ * 2);
      iZOffset = iPlaneBorderZ * 2;
    }
    else {
      iPlotWidth = iPeoplePlaneWidth / iNumberOfPeopleX;
      iXOffset = -iPlotWidth / 2;
      iPlotLength = iPeoplePlaneLength / iNumberOfPeopleZ;
      iZOffset = -iPlotLength / 2;
    }

    var iSpacingFactorX = Math.round(iPlotWidth / iMaxPeopleX);
    var iSpacingFactorZ = Math.round(iPlotLength / iMaxPeopleZ);

    var iPersonFixedY = PEOPLE_PLANE_THICKNESS / 2;

    var iNumberPlotted = 0;

    if (p_bDirect == true) {
      for (var j = 0; j < iMaxPeopleZ; j++) {
        for (var i = 0; i < iMaxPeopleX; i++) {
          if (iNumberPlotted < iNumberOfPeople) {
            var pPerson = p_arrPeople[iNumberPlotted];
            var pPersonGroup = createPerson(pPerson, p_bDirect)

            var x;
            var y;
            var z;

            x = i + iPeoplePlaneWidth / 2;
            //x = (i * iSpacingFactorX) + iXOffset;
            //x = randomizePosition(x, iSpacingFactorX);

            z = j + iPeoplePlaneLength / 2;
            //z = (j * iSpacingFactorZ) + iZOffset;
            //z = randomizePosition(z, iSpacingFactorZ);

            y = iPersonFixedY + (pPerson.height / 2);

            //Make sure person is plotted within boudaries of people plane
            if (x < iLeftEdge + (pPerson.width)) {
              x = iLeftEdge + (pPerson.width);
            }
            else if (x > iRightEdge - (pPerson.width)) {
              x = iRightEdge - (pPerson.width);
            }


            if (z > iBottomEdge - (pPerson.width)) {
              z = iBottomEdge - (pPerson.width);
            }
            else if (z < iTopEdge + (pPerson.width)) {
              z = iTopEdge + (pPerson.width);
            }


            pPersonGroup.position.x = x;
            pPersonGroup.position.y = y;
            pPersonGroup.position.z = z;

            //Save position and object to sheet data object
            var vPosition = { x: x, y: y, z: z };
            pPerson.position = vPosition;
            pPersonGroup.userData = { index: pPerson.index };

            if (pPerson.userId == userLoggedIn) {
              var firstName = p_arrPeople[iNumberPlotted].firstName;
              if (firstName == null || firstName == "")
                firstName = "?";

              var lastName = p_arrPeople[iNumberPlotted].lastName;
              if (lastName == null || lastName == "")
                lastName = "?";

              var text = "";
              var textMesh = null;
              if (firstName != null && lastName != null) {
                text = firstName + " " + lastName;
              }

              if (text != "") {
                var textReturn = createText(text, true, 30);
                var textObject = textReturn.obj;
                var textObjectWidth = textReturn.width;

                textObject.position.x = config.captionOffset.x;
                textObject.position.y = config.captionOffset.y;
                textObject.position.z = config.captionOffset.z;
                pPersonGroup.add(textObject);
              }

              //vMyPersonPosition = pPerson.position;
              pUserLoggedIn = pPerson;
              pMyPersonName = textObject;
            }

            peopleGroup.add(pPersonGroup);
            iNumberPlotted++;
          }
        }
      }
    }
    else {
      var fIncrement = (360 / iNumberOfPeople) + .1; //Because I don't trust floats
      for (var fAngle = 0; fAngle <= 360; fAngle += fIncrement) {
        if (iNumberPlotted < iNumberOfPeople) {
          var pPerson = p_arrPeople[iNumberPlotted];
          var fRadians = fAngle / (360 / (2 * Math.PI));
          var pPersonGroup = createPerson(pPerson, p_bDirect)

          var x;
          var y;
          var z;

          var fRadius = pPerson.width * 3;

          x = p_vSourcePosition.x + Math.cos(fRadians) * fRadius;
          x = randomizePosition(x, fRadius / 2);

          y = iPersonFixedY + (pPerson.height / 2);

          z = p_vSourcePosition.z + Math.sin(fRadians) * fRadius;
          z = randomizePosition(z, fRadius / 2);


          //Make sure person is plotted within boundaries of people plane
          if (x < iLeftEdge + (pPerson.width)) {
            x = iLeftEdge + (pPerson.width);
          }
          else if (x > iRightEdge - (pPerson.width)) {
            x = iRightEdge - (pPerson.width);
          }


          if (z > iBottomEdge - (pPerson.width)) {
            z = iBottomEdge - (pPerson.width);
          }
          else if (z < iTopEdge + (pPerson.width)) {
            z = iTopEdge + (pPerson.width);
          }


          pPersonGroup.position.x = x;
          pPersonGroup.position.y = y;
          pPersonGroup.position.z = z;

          var vPosition = { x: x, y: y, z: z };
          pPerson.position = vPosition;
          pPerson.renderObject = pPersonGroup;

          pPersonGroup.userData = { index: pPerson.index };

          peopleGroup.add(pPersonGroup);

          iNumberPlotted++;
        }
      }
    }
  }

  function populateSheets() {
    if (sheets.length > 1) {
      //Sort sheets ascending by create date
      sheets.sort(function (a, b) {
        a = new Date(a.created);
        b = new Date(b.created);
        return a > b ? 1 : a < b ? -1 : 0;
      });
    }

    var iNumberPlotted = 0;
    var iCellX;
    var iCellZ;
    var aCells = new Array();
    for (var i = 0; i < iMaxSheetX; i++) {
      aCells[i] = new Array();
      for (var j = 0; j < iMaxSheetZ; j++) {
        aCells[i][j] = 0;
      }
    }

    for (var j = 0; j < iMaxSheetZ; j++) {
      for (var i = 0; i < iMaxSheetX; i++) {
        if (iNumberPlotted < iNumberOfSheets) {

          //Find a cell to place this sheet in
          var canPlace = false;

          var fThreshold = 0.0;
          var fTier1Chance = 30.0;
          var fTier2Chance = 50.0;
          var fTier3Chance = 80.0;

          //var iTier1Threshold = parseint(iMaxSheetZ / 3);
          //var iTier2Threshold = parseint(iMaxSheetZ * 2 / 3);

          if (i == 0 || i == iMaxSheetZ - 1) {
            fThreshold = fTier1Chance;
          }
          else if ((i < iMaxSheetZ - 1) && (j == 0 || j == iMaxSheetX - 2)) {
            fThreshold = fTier2Chance;
          }
          else {
            fThreshold = fTier3Chance;
          }

          var fRand = Math.random() * 100;
          if (fRand < fThreshold) {
            canPlace = true;
          }

          //If this sheet can be placed in this cell, then create it, otherwise move on to the next cell
          if (canPlace == true) {
            var pSheet = sheets[iNumberPlotted];
            var pSheetRender = createSheet();
            iCellX = i;
            iCellZ = j;
            aCells[iCellX][iCellZ] = 1;

            var x;
            var y;
            var z;

            x = (iCellX * iSheetSpacingFactorX);
            pSheetRender.position.x = x;

            y = iSheetFixedY;
            pSheetRender.position.y = y;

            z = (iCellZ * iSheetSpacingFactorZ);
            pSheetRender.position.z = z;

            if (pSheet.ownerId == userLoggedIn) {
              //pSheetRender.material.color.setHex(0x518fb4);  //My Sheets are blue
              pSheetRender.material = pBlueSheetMat;
              iMySheets++; //Stat
              console.log("My " + iMySheets);
            }
            else {
              //pSheetRender.material.color.setHex(0xffffff);  //Other sheets are white
              pSheetRender.material = pWhiteSheetMat;
              iOtherSheets++; //Stat
              console.log("Other " + iOtherSheets);
            }

            //Save position and object to sheet data object
            var position = { x: x, y: y, z: z };
            pSheet.position = position;
            pSheet.renderObject = pSheetRender;

            sheetGroup.add(pSheetRender);

            iNumberPlotted++;
          }
        }
        else {
          var iActualSheetLength = j * iSheetSpacingFactorZ;
          var iOverFlow = iSheetsPlaneLength - iActualSheetLength;
          var iAdjustment = iOverFlow / 2;
          sheetGroup.position.z = sheetGroup.position.z + iAdjustment;
          break;
        }
      }
      if (iNumberPlotted >= iNumberOfSheets) break;
    }
  }

  function drawTimeline() {
    var geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50, peoplePlane.position.y - 200, peoplePlane.position.z - baseLength / 2));
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50, peoplePlane.position.y - 200, peoplePlane.position.z + baseLength / 2));
    var line = new THREE.Line(geometry, pGreyLineMat);
    masterGroup.add(line);

    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2, peoplePlane.position.y, peoplePlane.position.z + iPeoplePlaneLength / 2));
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50, peoplePlane.position.y - 200, peoplePlane.position.z + iPeoplePlaneLength / 2));
    line = new THREE.Line(geometry, pGreyLineMat);
    masterGroup.add(line);

    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2, peoplePlane.position.y, peoplePlane.position.z - iPeoplePlaneLength / 2));
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50, peoplePlane.position.y - 200, peoplePlane.position.z - iPeoplePlaneLength / 2));
    line = new THREE.Line(geometry, pGreyLineMat);
    masterGroup.add(line);

    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2, peoplePlane.position.y, peoplePlane.position.z - iPeoplePlaneLength * 0.15));
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50, peoplePlane.position.y - 200, peoplePlane.position.z - iPeoplePlaneLength * 0.15));
    line = new THREE.Line(geometry, pGreyLineMat);
    masterGroup.add(line);

    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2, peoplePlane.position.y, peoplePlane.position.z + iPeoplePlaneLength * 0.15));
    geometry.vertices.push(new THREE.Vector3(peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50, peoplePlane.position.y - 200, peoplePlane.position.z + iPeoplePlaneLength * 0.15));
    line = new THREE.Line(geometry, pGreyLineMat);
    masterGroup.add(line);

    var textReturn = createText("Now", false, 30);
    var textObject = textReturn.obj;
    var textObjectWidth = textReturn.width;

    textObject.material.color.setHex(0xFFFFFF);

    textObject.position.x = peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50;
    textObject.position.y = peoplePlane.position.y - 200;
    textObject.position.z = peoplePlane.position.z + iPeoplePlaneLength / 2;
    masterGroup.add(textObject);

    textReturn = createText(earliestYear, false, 30);
    textObject = textReturn.obj;
    textObjectWidth = textReturn.width;

    textObject.material.color.setHex(0xFFFFFF);

    textObject.position.x = peoplePlane.position.x - iPeoplePlaneWidth / 2 - 50;
    textObject.position.y = peoplePlane.position.y - 200;
    textObject.position.z = peoplePlane.position.z - iPeoplePlaneLength / 2;
    masterGroup.add(textObject);
  }


  function updateZoomIndicator() {
    //console.log("Zoom: " + controls.currentZoom);

    var fZoom = 0;
    var fIndicatorPosition = 0;
    var iPosition = 0;

    if (bEnableCameraControls)
    {
      fZoom = controls.currentZoom;
    }

    var arrPositions = new Array();
    //Zoomed In
    arrPositions[0] = -245;
    arrPositions[1] = -183;
    arrPositions[2] = -121;
    arrPositions[3] = -58; //Center
    arrPositions[4] = 4;
    arrPositions[5] = 67;
    arrPositions[6] = 130;
    //Zoomed Out

    if (fZoom >= -1.5 && fZoom < 0) {
      fIndicatorPosition = arrPositions[0];
    }
    else if (fZoom >= 0 && fZoom < .4) {
      fIndicatorPosition = arrPositions[1];
    }
    else if (fZoom >= .4 && fZoom < .8) {
      fIndicatorPosition = arrPositions[2];
    }
    else if (fZoom >= .8 && fZoom < 1.2) {
      fIndicatorPosition = arrPositions[3];
    }
    else if (fZoom >= 1.2 && fZoom < 1.4) {
      fIndicatorPosition = arrPositions[4];
    }
    else if (fZoom >= 1.4 && fZoom < 1.8) {
      fIndicatorPosition = arrPositions[5];
    }
    else if (fZoom >= 1.8 && fZoom <= 2) {
      fIndicatorPosition = arrPositions[6];
    }

    $(".zoom_selector").css("top", fIndicatorPosition + "px");
  }

  function updateSheets() {

  }

  function updatePeople() {
    //for (var i = 0; i < peopleGroup.children.length; i++)
    //{
    //    peopleGroup.children[i].scale.x = controls.currentZoom;
    //    peopleGroup.children[i].scale.y = controls.currentZoom;
    //    peopleGroup.children[i].scale.z = controls.currentZoom;
    //}
  }

  function onDocumentMouseDown(event) {
    if (bEventInProgress == false) {
      bEventInProgress = true;

      event.preventDefault();

      isMouseDown = true;

      //document.addEventListener('mousemove', onDocumentMouseMove, false);
      document.addEventListener('mouseup', onDocumentMouseUp, false);
      document.addEventListener('mouseout', onDocumentMouseOut, false);

      mouseXOnMouseDown = event.clientX - windowHalfX;
      targetRotationOnMouseDown = targetRotation;

      var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
      projector.unprojectVector(vector, camera);

      var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());

      var intersects = raycaster.intersectObjects(sheetGroup.children);

      if (intersects.length > 0) {
        //SM: If the sheet is not already selected, select it
        var bAlreadySelected = false;
        for (var i = 0; i < arrSheetsSelected.length; i++) {
          if (intersects[0].object == arrSheetsSelected[i].pSheet) {
            bAlreadySelected = true;

            var selectedSheet = arrSheetsSelected[i];

            //SM: Unselect the sheet
            selectedSheet.pSheet.scale.y = 1;

            if (selectedSheet.bOwnerSheet == true)
              selectedSheet.pSheet.material = pBlueSheetMat;
            else
              selectedSheet.pSheet.material = pWhiteSheetMat;

            var iConnectedSheets = selectedSheet.arrLinesConnected.length - 1; //Subtract 1 because the first line is from the sheet to the owner
            if (iConnectedSheets < 0) iConnectedSheets = 0;
            iSelectedConnections -= iConnectedSheets;  //Stat

            var iNumberOfAttachments = getNumberOfAttachments(sheets, selectedSheet.id);

            iSelectedAttachments -= iNumberOfAttachments; //Stat

            console.log("Id: " + selectedSheet.id + " Attachments: " + iNumberOfAttachments + " Selected: " + iSelectedAttachments);

            for (var j = 0; j < selectedSheet.arrLinesConnected.length; j++) {
              for (var k = 0; k < masterGroup.children.length; k++) {
                if (selectedSheet.arrLinesConnected[j] == masterGroup.children[k]) {
                  masterGroup.remove(selectedSheet.arrLinesConnected[j]);
                  break;
                }
              }
              delete selectedSheet.arrLinesConnected[j];
            }
            selectedSheet.arrLinesConnected.length = 0;
            delete selectedSheet.arrLinesConnected;

            //Delete the sheet name text display
            nameTextGroup.remove(selectedSheet.pNameTextDisplay);
            delete selectedSheet.pNameTextDisplay;
            selectedSheet.pNameTextDisplay = null;

            arrSheetsSelected.splice(i, i + 1);

            break;
          }
        }

        if (!bAlreadySelected)
          sheetSelected(intersects[0].object);
      }
      bEventInProgress = false;
    }
  }

  function onDocumentMouseMove(event) {
    for (var i = 0; i < sheetGroup.children.length; i++) {
      sheetGroup.children[i].material.opacity = 1.0;
      sheetGroup.children[i].material.transparent = false;
    }

    for (var i = 0; i < arrSheetsHovering.length; i++) {
      arrSheetsHovering[i].material = arrSheetsHoveringMats[i];
    }
    arrSheetsHovering.length = 0;
    arrSheetsHoveringMats.length = 0;

    if (pPersonHoveringMesh != null) {
      pPersonHoveringMesh.material = pPersonHoveringMeshMat;

    }

    //Delete the hovered sheet/person's name text. Text must be created new each time in Three.js.
    if (pObjectHoveringText) {
      nameTextGroup.remove(pObjectHoveringText);
      updateCurrent("", "", "", "");
      pObjectHoveringText.visible = false;
      delete pObjectHoveringText;
      pObjectHoveringText = null;
    }

    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;

    targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.02;

    if (isMouseDown == true)
      return;

    //Hover Functionality for sheets
    var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
    projector.unprojectVector(vector, camera);

    var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());

    var hitSheet = false;
    var intersects = raycaster.intersectObjects(sheetGroup.children);
    if (intersects.length > 0) {
      hitSheet = true;

      var pSheetHovering = intersects[0].object;

      for (var i = 0; i < sheetGroup.children.length; i++) {
        if (pSheetHovering == sheetGroup.children[i]) {
          var pSheetObject = sheetGroup.children[i];
          var pSheet = sheets[i];
          //Create name text object
          var text = pSheet.name;
          if (text == null)
            text = "";

          var textMesh = null;

          if (text != "") {
            var fDistance = camera.position.distanceToSquared(pSheetHovering.position);
            var fSize = 30;

            if (fDistance > 7000 * 7000)
              fSize = 60;
            else if (fDistance > 6000 * 6000)
              fSize = 50;
            else if (fDistance > 5000 * 5000)
              fSize = 40;
            else
              fSize = 30;

            var textReturn = createText(text, true, fSize);
            updateCurrent(text, pSheet.collaborators.length, pSheet.created, "Sheet");
            pObjectHoveringText = textReturn.obj;
            var textObjectWidth = textReturn.width;

            nameTextGroup.add(pObjectHoveringText);

            pObjectHoveringText.position.x = sheetGroup.position.x + pSheetObject.position.x;
            pObjectHoveringText.position.y = sheetGroup.position.y + pSheetObject.position.y + 100;
            pObjectHoveringText.position.z = sheetGroup.position.z + pSheetObject.position.z;

            pObjectHoveringText.visible = true;

            break;
          }
        }
      }
    }

    //Hover Functionality for people
    //Don't raycast to the people if we have hit a sheet
    if (!hitSheet) {
      for (var i = 0; i < peopleGroup.children.length; i++) {
        intersects = raycaster.intersectObject(peopleGroup.children[i].children[0]);

        if (intersects.length > 0) {
          pPersonHoveringMesh = intersects[0].object;

          var pPersonObject = peopleGroup.children[i];
          var pPerson = people[pPersonObject.userData.index];
          pPersonHoveringMeshMat = pPersonHoveringMesh.material;
          pPersonHoveringMesh.material = pGreenPersonMat;

          //Create name text object
          var firstName = pPerson.firstName;
          if (firstName == null || firstName == "") {
            firstName = "";
          }

          var lastName = pPerson.lastName;
          if (lastName == null || lastName == "") {
            lastName = "";
          }

          var text = "";
          var textMesh = null;
          if (firstName != null && lastName != null) {
            text = firstName + " " + lastName;
          }

          updateCurrent(text, pPerson.shares, pPerson.firstShared, "User");
          if (people[pPersonObject.userData.index].userId != userLoggedIn) {
            if (text != "") {
              var textReturn = createText(text, true, 30);
              pObjectHoveringText = textReturn.obj;
              var textObjectWidth = textReturn.width;

              nameTextGroup.add(pObjectHoveringText);

              pObjectHoveringText.position.x = peopleGroup.position.x + pPersonObject.position.x /*- (textObjectWidth / 2)*/;
              pObjectHoveringText.position.y = peopleGroup.position.y + pPersonObject.position.y + 100;
              pObjectHoveringText.position.z = peopleGroup.position.z + pPersonObject.position.z;
            }
          }

          //Find the sheets that this person owns
          var bSheetFound = false;
          for (var k = 0; k < sheetGroup.children.length; k++) {
            if (pPerson.userId == sheets[k].ownerId) {
              bSheetFound = true;
              arrSheetsHoveringMats.push(sheetGroup.children[k].material);
              sheetGroup.children[k].material = pGreenSheetMat;
              arrSheetsHovering.push(sheetGroup.children[k]);
            }
            else {
              sheetGroup.children[k].material.transparent = true;
              sheetGroup.children[k].material.opacity = 0.1;
            }
          }

          if (bSheetFound == false) {
            for (var k = 0; k < sheetGroup.children.length; k++) {
              sheetGroup.children[k].material.transparent = false;
              sheetGroup.children[k].material.opacity = 1.0;
            }
          }

          return;
        }
      }
    }
    else {
      pPersonHoveringMesh = null;
    }
  }

  function onDocumentMouseUp(event) {
    isMouseDown = false;

    //document.removeEventListener('mousemove', onDocumentMouseMove, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);
  }

  function onDocumentMouseOut(event) {
    //document.removeEventListener('mousemove', onDocumentMouseMove, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);
  }

  function onDocumentTouchStart(event) {

    if (event.touches.length === 1) {

      event.preventDefault();

      mouseXOnMouseDown = event.touches[0].pageX - windowHalfX;
      targetRotationOnMouseDown = targetRotation;

    }

  }

  function onDocumentTouchMove(event) {

    if (event.touches.length === 1) {

      event.preventDefault();

      mouseX = event.touches[0].pageX - windowHalfX;
      targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.05;
    }
  }

  function onWindowResize() {
    var height = window.innerHeight;
    var width = window.innerWidth;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);
  }

  // UPDATE LOOP
  // Update is executed on each frame
  function update() {
    if (bEnableCameraControls == true) {
      controls.update();
    }
    else if (bUpdatedOnce == false) {
      bUpdatedOnce = true;
      controls.update();
    }

    updateSheets();

    updatePeople();

    updateZoomIndicator();

    updateStats();

    //Update each person so they point at the camera 
    for (i = 0; i < peopleGroup.children.length; i++) {
      peopleGroup.children[i].rotation.setFromRotationMatrix(camera.matrix, "YXZ");
      peopleGroup.children[i].rotation.x = 0;
      peopleGroup.children[i].rotation.z = 0;
    }

    //Update each person so they point at the camera 
    for (i = 0; i < nameTextGroup.children.length; i++) {
      nameTextGroup.children[i].rotation.setFromRotationMatrix(camera.matrix, "YXZ");
      nameTextGroup.children[i].rotation.x = 0;
      nameTextGroup.children[i].rotation.z = 0;
    }

    renderer.render(scene, camera);

    // request new frame
    requestAnimationFrame(function () {
      update();
    });
  }

  //UTILITIES

  //Randomize a position
  function randomizePosition(value, margin) {
    var adjustedValue = value;
    var choice = Math.floor(Math.random() * 2);
    if (choice == 0) {
      adjustedValue -= (Math.random() * margin) / 2;
    } else {
      adjustedValue += (Math.random() * margin) / 2;
    }
    return adjustedValue;
  }

  function createArrows() {
    var pArrowGroup = new THREE.Object3D();
    var pTexture = null;

    pTextureRightArrow = THREE.ImageUtils.loadTexture("resources/images/rotate_right.png");
    pTextureBottomArrow = THREE.ImageUtils.loadTexture("resources/images/rotate_right.png");
    pTextureLeftArrow = THREE.ImageUtils.loadTexture("resources/images/rotate_right.png");

    var pMaterialRightArrow = new THREE.MeshBasicMaterial({ transparent: true, map: pTextureRightArrow, overdraw: true });
    var pMaterialBottomArrow = new THREE.MeshBasicMaterial({ transparent: true, map: pTextureBottomArrow, overdraw: true });
    var pMaterialLeftArrow = new THREE.MeshBasicMaterial({ transparent: true, map: pTextureLeftArrow, overdraw: true });

    pMaterialRightArrow.setValues({ side: THREE.DoubleSide });
    pMaterialLeftArrow.setValues({ side: THREE.DoubleSide });
    pMaterialBottomArrow.setValues({ side: THREE.DoubleSide });

    pMeshRightArrow = new THREE.Mesh(new THREE.PlaneGeometry(358, 380), pMaterialRightArrow);
    pMeshBottomArrow = new THREE.Mesh(new THREE.PlaneGeometry(358, 380), pMaterialBottomArrow);
    pMeshLeftArrow = new THREE.Mesh(new THREE.PlaneGeometry(358, 380), pMaterialLeftArrow);


    pMeshBottomArrow.position.x = peoplePlane.position.x - (iPeoplePlaneWidth / 2) - ROTATE_ARROW_SPACING;;
    pMeshBottomArrow.position.y = peoplePlane.position.y;
    pMeshBottomArrow.position.z = peoplePlane.position.z + (iPeoplePlaneLength / 2) + ROTATE_ARROW_SPACING;

    pMeshLeftArrow.position.x = peoplePlane.position.x - (iPeoplePlaneWidth / 2) - ROTATE_ARROW_SPACING;
    pMeshLeftArrow.position.y = peoplePlane.position.y;
    pMeshLeftArrow.position.z = peoplePlane.position.z - (iPeoplePlaneLength / 2) - ROTATE_ARROW_SPACING;;

    pMeshRightArrow.position.x = (peoplePlane.position.x + iPeoplePlaneWidth / 2) + ROTATE_ARROW_SPACING;
    pMeshRightArrow.position.y = peoplePlane.position.y;
    pMeshRightArrow.position.z = (peoplePlane.position.z + iPeoplePlaneLength / 2) + ROTATE_ARROW_SPACING;

    pMeshRightArrow.rotateX(Math.PI / 2);
    pMeshRightArrow.rotateZ(Math.PI / 8);

    pMeshLeftArrow.rotateX(Math.PI / 2);
    pMeshLeftArrow.rotateZ(Math.PI * 1.1);

    pMeshBottomArrow.rotateX(Math.PI / 2);
    pMeshBottomArrow.rotateZ(Math.PI * .6);

  }

  //Person Object Creation
  function createPerson(p_pPerson, p_bDirect) {
    var pPersonGroup = new THREE.Object3D();

    var pTexture = null;

    pTexture = THREE.ImageUtils.loadTexture("resources/images/Person_blue.png");

    var pMaterial = new THREE.MeshBasicMaterial({ transparent: true, map: pTexture, overdraw: true });

    p_pPerson.height = 150;
    p_pPerson.width = 75;

    pPersonGroup.add(pMesh);
    return pPersonGroup;
  }

  function createSheet() {
    var pMesh = new THREE.Mesh(new THREE.CubeGeometry(90, 45, 90), new THREE.MeshLambertMaterial(
            {
              color: 0x518fb4 //blue
              //color: 0xd5a728 //yellow
            }
        ));

    return pMesh;
  }

  function createText(text, background, p_iSize) {
    var textGroup = new THREE.Object3D();

    var text3d = new THREE.TextGeometry(text, {

      size: p_iSize,
      height: 5,
      curveSegments: 20,
      font: "helvetiker"

    });

    text3d.computeBoundingBox();
    var textWidth = (text3d.boundingBox.max.x - text3d.boundingBox.min.x);
    var textHeight = (text3d.boundingBox.max.y - text3d.boundingBox.min.y);
    var centerOffset = -0.5 * textWidth;

    var textMaterial = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, overdraw: true });
    textMesh = new THREE.Mesh(text3d, textMaterial);
    textMesh.position.x = centerOffset;
    textMesh.position.y = 100;
    textMesh.position.z = 0;
    textMesh.rotation.x = 0;
    textMesh.rotation.y = Math.PI * 2;

    var backgroundWidth = textWidth * 1.2;
    var backgroundHeight = textHeight * 1.6;

    if (background == true) {
      var backgroundMaterial = new THREE.MeshBasicMaterial({ color: 0x4C4E51, overdraw: true });
      backgroundMesh = new THREE.Mesh(new THREE.PlaneGeometry(backgroundWidth, backgroundHeight), backgroundMaterial);
      backgroundMesh.position.x = 0;
      backgroundMesh.position.y = 100 + (backgroundHeight * 0.25);
      backgroundMesh.position.z = 0;
      backgroundMesh.rotation.x = 0;
      backgroundMesh.rotation.y = Math.PI * 2;

      backgroundMaterial.transparent = true;
      backgroundMaterial.opacity = 0.8;

      textGroup.add(backgroundMesh);
      textGroup.add(textMesh);

      return { obj: textGroup, width: backgroundWidth, height: backgroundHeight };
    }

    return { obj: textMesh, width: backgroundWidth, height: backgroundHeight };
  }

  function sheetSelected(p_pMesh) {
    ///////////////////////////////////////////////////////////////////////////////////
    //Find the sheet data that matches the sheet object
    ///////////////////////////////////////////////////////////////////////////////////
    var pSheetData = null;
    for (var i = 0; i < sheetGroup.children.length; i++) {
      if (sheetGroup.children[i] == p_pMesh) {
        pSheetData = sheets[i];
        break;
      }
    }

    if (pSheetData == null)
      return;

    var iNumberOfAttachments = getNumberOfAttachments(sheets, pSheetData.id);

    iSelectedAttachments += iNumberOfAttachments;

    console.log("Id: " + pSheetData.id + " Attachments: " + iNumberOfAttachments + " Selected: " + iSelectedAttachments);

    ///////////////////////////////////////////////////////////////////////////////////
    //Find the person object that owns the sheet
    ///////////////////////////////////////////////////////////////////////////////////
    var pPerson = null;
    for (var i = 0; i < people.length; i++) {
      if (people[i].userId == pSheetData.ownerId) {
        pPerson = people[i];
        break;
      }
    }

    if (pPerson == null)
      return;

    var selectedSheet = new SelectedSheet();
    selectedSheet.pSheet = p_pMesh;
    selectedSheet.id = pSheetData.id;

    if (pSheetData.ownerId == userLoggedIn)
      selectedSheet.bOwnerSheet = true;
    else
      selectedSheet.bOwnerSheet = false;

    arrSheetsSelected.push(selectedSheet);

    ///////////////////////////////////////////////////////////////////////////////////
    ////Create name text object
    ///////////////////////////////////////////////////////////////////////////////////
    var text = pSheetData.name;
    if (text == null)
      text = "";

    var textMesh = null;

    if (text != "") {
      var fDistance = camera.position.distanceToSquared(p_pMesh.position);
      var fSize = 30;

      if (fDistance > 7000 * 7000)
        fSize = 60;
      else if (fDistance > 6000 * 6000)
        fSize = 50;
      else if (fDistance > 5000 * 5000)
        fSize = 40;
      else
        fSize = 30;

      var textReturn = createText(text, true, fSize);
      updateCurrent(text, pSheetData.collaborators.length, pSheetData.created, "Sheet");
      var pNameDisplay = textReturn.obj;
      var textObjectWidth = textReturn.width;

      nameTextGroup.add(pNameDisplay);
      selectedSheet.pNameTextDisplay = pNameDisplay;

      pNameDisplay.position.x = sheetGroup.position.x + p_pMesh.position.x;
      pNameDisplay.position.y = sheetGroup.position.y + p_pMesh.position.y + 100;
      pNameDisplay.position.z = sheetGroup.position.z + p_pMesh.position.z;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    //Create a line from the sheet to the owner
    ///////////////////////////////////////////////////////////////////////////////////

    p_pMesh.material = pYellowSheetMat;
    //p_pMesh.scale.y = 5;

    var geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(sheetGroup.position.x + p_pMesh.position.x, sheetGroup.position.y + p_pMesh.position.y, sheetGroup.position.z + p_pMesh.position.z));
    geometry.vertices.push(new THREE.Vector3(peopleGroup.position.x + pPerson.position.x, peopleGroup.position.y + pPerson.position.y - (pPerson.height / 2), peopleGroup.position.z + pPerson.position.z));

    var line = new THREE.Line(geometry, pYellowLineMat);

    masterGroup.add(line);
    selectedSheet.arrLinesConnected.push(line);

    ///////////////////////////////////////////////////////////////////////////////////
    //Find the collaberators
    ///////////////////////////////////////////////////////////////////////////////////
    var arrCollaboratorIds = new Array();
    for (var i = 0; i < pSheetData.collaborators.length; i++) {
      arrCollaboratorIds.push(pSheetData.collaborators[i].userId);
    }

    var arrCollaboratorObjects = new Array();
    for (var i = 0; i < arrCollaboratorIds.length; i++) {
      for (var j = 0; j < people.length; j++) {
        if (arrCollaboratorIds[i] == people[j].userId) {
          arrCollaboratorObjects.push(people[j]);
          break;
        }
      }
    }

    ///////////////////////////////////////////////////////////////////////////////////
    //Create a line from the owner to each collaborator
    ///////////////////////////////////////////////////////////////////////////////////
    for (var i = 0; i < arrCollaboratorObjects.length; i++) {

      var pCollaborator = arrCollaboratorObjects[i];

      if (pCollaborator != null && pCollaborator.position != null && peopleGroup != null && pPerson != null && pPerson.position != null && pCollaborator.userId != pPerson.userId) {
        iSelectedConnections++;  //Stat
        var geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(peopleGroup.position.x + pPerson.position.x, peopleGroup.position.y + pPerson.position.y - (pPerson.height / 2) + 1, peopleGroup.position.z + pPerson.position.z));
        geometry.vertices.push(new THREE.Vector3(peopleGroup.position.x + pCollaborator.position.x, peopleGroup.position.y + pCollaborator.position.y - (pCollaborator.height / 2) + 1, peopleGroup.position.z + pCollaborator.position.z));
        var line = new THREE.Line(geometry, pYellowLineMat);
        masterGroup.add(line);
        selectedSheet.arrLinesConnected.push(line);
      }
    }
  }

  function drawGrayLines(p_arrPeople, p_pSourcePerson, p_bDirect) {
    return;
    for (var i = 0; i < p_arrPeople.length; i++) {
      pPerson = p_arrPeople[i];
      if (pPerson != null && pPerson.userId != null && p_pSourcePerson != null && p_pSourcePerson.userId != null) {
        if (pPerson.userId != p_pSourcePerson.userId) {
          var geometry = new THREE.Geometry();
          if (peopleGroup != null && peopleGroup.position != null && p_pSourcePerson.position != null && p_pSourcePerson.height != null) {
            geometry.vertices.push(new THREE.Vector3(peopleGroup.position.x + p_pSourcePerson.position.x, peopleGroup.position.y + p_pSourcePerson.position.y - (p_pSourcePerson.height / 2), peopleGroup.position.z + p_pSourcePerson.position.z));
            geometry.vertices.push(new THREE.Vector3(peopleGroup.position.x + pPerson.position.x, peopleGroup.position.y + pPerson.position.y - (pPerson.height / 2), peopleGroup.position.z + pPerson.position.z));
            var line = new THREE.Line(geometry, pGreyLineMat);
            masterGroup.add(line);

            if (p_bDirect == true) {
              iDirectConnections++;
            }
            else {
              iIndirectConnections++;
            }
          }
        }
      }
    }
  }

  function updateStats() {

    iSelectedSheets = arrSheetsSelected.length;

    $("#stat_total_sheets").text(iTotalSheets);
    $("#stat_my_sheets").text(iMySheets);
    $("#stat_other_sheets").text(iOtherSheets);

    $("#stat_total_connections").text(iTotalConnections);
    $("#stat_direct_connections").text(iDirectConnections);
    $("#stat_indirect_connections").text(iIndirectConnections);

    $("#stat_selected_sheets").text(iSelectedSheets);
    $("#stat_selected_connections").text(iSelectedConnections);
    $("#stat_selected_attachments").text(iSelectedAttachments);
  }

  function updateCurrent(p_strText, p_intShares, p_strDate, p_strType) {
    if (DEBUG_SHOW_HOVER_DATA) {
      $("#billboard_item").text(p_strType + '(' + p_intShares + ')');
      $("#billboard_name").text(p_strText);
      $("#billboard_date").text(p_strDate);

    }

  }


  //Turn off the carousel we are ready to go!
  $("#carousel_block").css("display", "none");
  //Start update
  setTimeout(function () { update(); }, 500);
}

function setCameraControls(p_bValue) {
  bEnableCameraControls = !p_bValue;
}

function onKeyPress(key) {

  var value = key.keyCode;

  if(value == 87) {             //W

  } else if(value == 65){       //A

  } else if(value == 83){       //S

  } else if (value == 68) {     //D

  }
}