﻿var strMode = "";

function logout() {
    sessionStorage["fetchBot3d.authorized"] = "false";
    sessionStorage["fetchBot3d.userEmail"] = "";
    sessionStorage["debug.dataset"] = -1;
    window.location = "Default.html";
}

function showMap()
{
    setTimeout(function () { window.location="Game.html"; }, 500);
}

function initHeader()
{
    var strEmail = sessionStorage["fetchBot3d.userEmail"];
    //console.log("Init Header: " + strEmail);
    $("#user_email").text(strEmail);

}

var bOptionsOpen = false;

function SwitchOptions() {
    if (bOptionsOpen == false) {
        $("#logout").css("display", "block");
        $("#options_arrow_image").attr("src", "resources/images/options_arrow_down.png");
        $(".options_arrow").css("top", "15px");
        $(".options_arrow").css("right", "48px");
        bOptionsOpen = true;
    } else {
        $("#logout").css("display", "none");
        $("#options_arrow_image").attr("src", "resources/images/options_arrow_right.png");
        $(".options_arrow").css("top", "12px");
        $(".options_arrow").css("right", "43px");
        bOptionsOpen = false;
    }
}

function GenerateEmail()
{
    //console.log("Generating Email");



    //Temporariliy turn off email button
    $('.email_button_wide').css('display', 'none');
    $('.download_button_wide').css('display', 'none');



    //var pShare = window.open("");
    //Convert the page to a canvas
    html2canvas(document.body,
        {
            onrendered:
                function (canvas)
                {

                    //Convert the canvas to a PNG
                    var rawImageData = canvas.toDataURL("image/png;base64");
                    rawImageData = rawImageData.replace("image/png", "image/octet-stream");
                    parent.location = 'mailto:rmadsen@iname.com?subject=My WorkMap&body=' + rawImageData;



                    //var pImage = pShare.document.createElement("img");
                    //pImage.setAttribute('src', img);



                    //Add the image to the new window
                    //pShare.document.body.appendChild(pImage);

                    ////Create a button
                    //var pShareButton = pShare.document.createElement("img");

                    //var pLocation = parseLocation(window.location);
                    //var strProtocol = pLocation.protocol;
                    //var strHost = pLocation.host;

                    //pShareButton.setAttribute("src", strProtocol + "//" + strHost + "/resources/images/share_button.png");
                    //pShareButton.setAttribute("alt", "Share");
                    ////And an anchor
                    //var pAnchor = pShare.document.createElement("a");
                    //pAnchor.setAttribute("onclick", "alert('clicked');");
                    //pAnchor.setAttribute("style", "position: relative; display: block; top: -100%; margin: 0 auto 0 auto;");
                    ////Append the button
                    //pAnchor.appendChild(pShareButton);

                    ////Now add the anchor/button to the document
                    //pShare.document.body.appendChild(pAnchor);

                }
        }
    );

    //Turn the email button back on
    setTimeout(function () { $('.email_button_wide').css('display', 'block'); }, 500);
    setTimeout(function () { $('.download_button_wide').css('display', 'block'); }, 500);


}

function GenerateDownload()
{
    //console.log("Downloading image");

    //Temporariliy turn off email button
    $('.email_button_wide').css('display', 'none');
    $('.download_button_wide').css('display', 'none');

    //Convert the page to a canvas
    html2canvas(document.body,
        {
            onrendered:
                function (canvas)
                {

                    //Convert the canvas to a PNG
                    var rawImageData = canvas.toDataURL("image/png;base64");
                    rawImageData = rawImageData.replace("image/png", "image/octet-stream");
                    document.location.href = rawImageData;
                }
        }
    );

    //Turn the email button back on
    setTimeout(function () { $('.email_button_wide').css('display', 'block'); }, 500);
    setTimeout(function () { $('.download_button_wide').css('display', 'block'); }, 500);

}


function DownloadImage(imageURL)
{
    var oImage = document.getElementById(imageURL);
    var canvas = document.createElement("canvas");
    document.body.appendChild(canvas);
    if (typeof canvas.getContext == "undefined" || !canvas.getContext)
    {
        alert("browser does not support this action, sorry");
        return false;
    }

    try
    {
        var context = canvas.getContext("2d");
        var width = oImage.width;
        var height = oImage.height;
        canvas.width = width;
        canvas.height = height;
        canvas.style.width = width + "px";
        canvas.style.height = height + "px";
        context.drawImage(oImage, 0, 0, width, height);
        var rawImageData = canvas.toDataURL("image/png;base64");
        rawImageData = rawImageData.replace("image/png", "image/octet-stream");
        document.location.href = rawImageData;
        document.body.removeChild(canvas);
    }
    catch (err)
    {
        document.body.removeChild(canvas);
        alert("Sorry, can't download");
    }

    return true;
}

var strUserAgent;
var iScreenHeight;
var iScreenWidth;

window.addEventListener('resize', onWindowResize, false);

function onWindowResize()
{
    repaintInterface();
}


function initScreen(p_strMode)
{
    strMode = p_strMode;

    if (p_strMode == 'mobile')
    {
        setupMobileFull();
    }
    else if (p_strMode == 'web')
    {
        setupScreen("#window", "#page", "#header_block", null, null, null, "#carousel_block", p_strMode);
    }
    else if (p_strMode == '3D')
    {
        setupScreen("#workmap_window", "#workmap_page", "#workmap_header_block", null, null, "#stat_panel_block", "#carousel_block", p_strMode);
    }

}

function setupMobileFull(p_strWindowContainerId, p_strPageContainerId)
{

    var pWindowContainer = $(p_strWindowContainerId);
    var pPageContainer = $(p_strPageContainerId);

    iScreenHeight = window.innerHeight,
	iScreenWidth = window.innerWidth;

    //Making the container taller than the screen keeps the address bar from popping up
    iScreenHeight = window.innerHeight + 10;
    pWindowContainer.css({ height: iScreenHeight, width: iScreenWidth, padding: 0, margin: 0 });

    //This command tells the Ios address bar to close
    window.scrollTo(0, 1);

    pPageContainer.attr({ width: iScreenWidth, height: iScreenHeight });
    pPageContainer.css({ position: 'absolute', left: 0, top: 0 });

    //While we are at it, let's get the device
    strUserAgent = navigator.userAgent.toLowerCase();
    //console.log("Device:", userAgent);

}


function setupScreen(p_strWindowContainerId, p_strPageContainerId, p_strTopContainerId, p_strLeftContainerId, p_strBottomContainerId, p_strRightContainerId, p_strCenterContainerId, p_strMode)
{
    var pWindowContainer = null;
    var pPageContainer = null;
    var pTopContainer = null;
    var pLeftContainer = null;
    var pRightContainer = null;
    var pBottomContainer = null;
    var pCenterContainer = null;

    if(p_strWindowContainerId) pWindowContainer = $(p_strWindowContainerId);
    if(p_strPageContainerId) pPageContainer = $(p_strPageContainerId);
    if(p_strTopContainerId) pTopContainer = $(p_strTopContainerId);
    if(p_strLeftContainerId) pLeftContainer = $(p_strLeftContainerId);
    if(p_strRightContainerId) pRightContainer = $(p_strRightContainerId);
    if (p_strBottomContainerId) pBottomContainer = $(p_strBottomContainerId);
    if (p_strCenterContainerId) pCenterContainer = $(p_strCenterContainerId);

    var pWindowClass = null;
    var pPageClass = null;
    var pTopClass = null;
    var pLeftClass = null;
    var pRightClass = null;
    var pBottomClass = null;
    var pCenterClass = null;

    if (pWindowContainer) pWindowClass = $("." + p_strWindowContainerId.substr(1));
    if (pPageContainer) pPageClass = $("." + p_strPageContainerId.substr(1));
    if (pTopContainer) pTopClass = $("." + p_strTopContainerId.substr(1));
    if (pLeftContainer) pLeftClass = $("." + p_strLeftContainerId.substr(1));
    if (pRightContainer) pRightClass = $("." + p_strRightContainerId.substr(1));
    if (pBottomContainer) pBottomClass = $("." + p_strBottomContainerId.substr(1));
    if (pCenterContainer) pCenterClass = $("." + p_strCenterContainerId.substr(1));


    var iClientWidth = document.documentElement.clientWidth;
    var iClientHeight = document.documentElement.clientHeight;

    var iPageWidth = 0;
    var iPageHeight = 0;
    var iTopWidth = 0;
    var iTopHeight = 0;
    var iLeftWidth = 0;
    var iLeftHeight = 0;
    var iBottomWidth = 0;
    var iBottomHeight = 0;
    var iRightWidth = 0;
    var iRightHeight = 0;
    var iCenterWdith = 0;
    var iCenterHeight = 0;

    if (pPageClass)
    {
        iPageWidth = eval(pPageClass.css('width').replace('px', ''));
        iPageHeight = eval(pPageClass.css('height').replace('px', ''));
    }

    if (pTopClass)
    {
        iTopWidth = eval(pTopClass.css('width').replace('px', ''));
        iTopHeight = eval(pTopClass.css('height').replace('px', ''));
    }

    if (pLeftClass)
    {
        iLeftWidth = eval(pLeftClass.css('width').replace('px', ''));
        iLeftHeight = eval(pLeftClass.css('height').replace('px', ''));
    }

    if (pBottomClass)
    {
        iBottomWidth = eval(pBottomClass.css('width').replace('px', ''));
        iBottomHeight = eval(pBottomClass.css('height').replace('px', ''));
    }

    if (pRightClass)
    {
        iRightWidth = eval(pRightClass.css('width').replace('px', ''));
        iRightHeight = eval(pRightClass.css('height').replace('px', ''));
    }

    if (pCenterClass)
    {
        iCenterWidth = eval(pCenterClass.css('width').replace('px', ''));
        iCenterHeight = eval(pCenterClass.css('height').replace('px', ''));
    }


    //Setup window to take up full height and width of screen
    iScreenHeight = window.innerHeight;
    iScreenWidth = window.innerWidth;

    if (pWindowContainer != null)
    {
        pWindowContainer.css({ height: iScreenHeight, width: iScreenWidth, padding: 0, margin: 0 });
    }

    //Setup page position to be centered at top
    if (pPageClass != null)
    {
        pPageClass.css({
            "position": "absolute",
            "left": 0,
            "top": 0,
            "width": iScreenWidth.toString() + "px",
            "height": iScreenHeight.toString() + "px"
        });
    }

    //Setup top panel position to be centered top
    if (pTopClass!= null)
    {
        pTopClass.css({
            "position": "absolute",
            "left": 0,
            "top": 0,
            "width": iScreenWidth.toString() + "px"
        });

        var strBackgroundId = p_strTopContainerId + "_background";
        var pBackground = $(strBackgroundId);
        pBackground.attr("width", iClientWidth);
        if (p_strMode == "web")
        {
            pBackground.attr("height", iClientHeight);
        }
        else
        {
            pBackground.attr("height", iTopHeight);
        }
    }

    //Setup left panel position to be aligned to right
    if (pLeftClass!= null)
    {
        pLeftClass.css({
            "position": "absolute",
            "left": 0,
            "top" : (iPageHeight - iBottomHeight).toString() + "px"
        });

        if (iScreenHeight < iLeftHeight)
        {
            pLeftClass.css("overflow-y", "auto");
        }
        else
        {
            pLeftClass.css("overflow-y", "hidden");
        }
    }

    //Setup bottom panel position to be centered bottom and spread the width of the screen
    if (pBottomClass!= null)
    {
        pBottomClass.css({
            "position": "absolute",
            "left": 0,
            "top": (iPageHeight - iBottomHeight).toString() + "px",
            "width": iScreenWidth.toString() + "px"
        });
        var strBackgroundId = p_strBottomContainerId + "_background";
        var pBackground = $(strBackgroundId);
        pBackground.attr("width", iClientWidth);
        pBackground.attr("height", iBottomHeight);
    }

    //Setup right panel position to far right
    if (pRightClass != null)
    {
        pRightClass.css({
            "position": "absolute",
            "left": (iScreenWidth - iRightWidth).toString() + "px",
            "top": iTopHeight.toString() + "px"
        });

        if (iScreenHeight < iRightHeight)
        {
            pRightClass.css("overflow-y", "auto");
        }
        else
        {
            pRightClass.css("overflow-y", "hidden");
        }
    }

    //Setup center panel position to center screen
    if (pCenterClass != null)
    {
        pCenterClass.css({
            "position": "absolute",
            "left": ((iScreenWidth / 2) - (iCenterWidth / 2)).toString() + "px",
            "top": ((iScreenHeight / 2) - (iCenterHeight / 2)).toString() + "px"
        });
    }


    //While we are at it, let's get the device
    strUserAgent = navigator.userAgent.toLowerCase();
    //console.log("Device:", userAgent);


}

function repaintInterface()
{
    initScreen(strMode);
}


function initSearch()
{
    $("#search_text").focus();
}