﻿//Global stats
var iNumberOfDirectConnections;
var iNumberOfIndirectConnections;
var arrSortedSheetsByName;

//Given a user id from the sheets, update the share count on people
function assignSharesToUser(count, userId, people ) {
    
    for(var i = 0; i < people.length; i++)
    {
        if (people[i].userId == userId) {
            //Create the property if it doesn't exist
            if (people[i].shares == null) {
                people[i].shares = 0;
            }
            //Now increment the count
            people[i].shares += count;
            break;
        }
    }
}

//Iterate through the sheets add share counts
function accumulateShares(sheets, people) {

    for (var i = 0; i < sheets.length; i++) {
        assignSharesToUser(sheets[i].collaborators.length, sheets[i].ownerId, people);
    }
}

function assignFavoritesToUser(ownerId, sheets, favoritesThreshold)
{
    var arrMySheets = new Array();
    if (sheets.length > 1)
    {
        //Sort sheet descending by load count
        sheets.sort(function (a, b) { return b.loadCount - a.loadCount; });
    }

    //Find the top 15 sheets with the highest loadcounts
    var arrMyTop15 = new Array();
    var iHighestLoadCount = 0;
    var bAlreadyFound = false;
    var iMaxTopSheets = 15;
    if (sheets.length < iMaxTopSheets) iMaxTopSheets = sheets.length;
    for (var i = 0; i < iMaxTopSheets; i++)
    {
        arrMyTop15.push(sheets[i]);
    }

    //Sort the array by loadCount descending
    if (arrMyTop15.length > 1)
    {
        arrMyTop15.sort(function (a, b) { return b.loadCount - a.loadCount; });
    }

    //Parse the top 15 sheets for the 3 with the highest loadCount from unique workspaces
    var arrSheetsByUniqueWorkspace = new Array();
    var strWorkspace = "";
    var bWorkspaceFound = false;
    for (var i = 0; i < arrMyTop15.length; i++)
    {
        strWorkspace = arrMyTop15[i].workspace;
        bWorkspaceFound = false;

        for (var j = 0; j < arrSheetsByUniqueWorkspace.length; j++)
        {
            if (strWorkspace == arrSheetsByUniqueWorkspace[j].workspace)
            {
                bWorkspaceFound = true;
                break;
            }
        }

        if( bWorkspaceFound == false )
            arrSheetsByUniqueWorkspace.push(arrMyTop15[i]);
    }

    //Create the favorite property for top 3 sheets
    for (var i = 0; i < arrSheetsByUniqueWorkspace.length; i++)
    {
        //console.log("FavSheetFound - loadCount: " + arrSheetsByUniqueWorkspace[i].loadCount + " - Workspace: " + arrSheetsByUniqueWorkspace[i].workspace);

        if (arrSheetsByUniqueWorkspace[i].favorite == null)
        {
            arrSheetsByUniqueWorkspace[i].favorite = false;
        }
        arrSheetsByUniqueWorkspace[i].favorite = true;

        if (i >= (favoritesThreshold - 1))
            break;
    }
}

//Returns an array of unique sheet owners not inluding the logged in user
function extractUniqueSheetOwners(p_iLoggedInUserId, sheets, people)
{
    numberOfDirectConnections = 0;
    var arrSheetOwners = new Array();
    for(var i = 0; i < sheets.length; i++)
    {
        //Find the owner of this sheet
        var pSheet = sheets[i];
        var iSheetOwnerId = pSheet.ownerId;
        
        //See if this owner is one of the people in the dataset
        for (var j = 0; j < people.length; j++) {
            pPerson = people[j];
            console.log(pSheet.ownerId + " " + pSheet.name + " | " + pPerson.userId + " " + pPerson.firstName);
            if (pPerson.userId == iSheetOwnerId) {
                console.log("Owner!");
                //If this person hasn't already been marked as a sheet owner, add them to the owners array
                if (pPerson.isSheetOwner == null || pPerson.isSheetOwner == false) {
                    pPerson.isSheetCollaborator = false;
                    pPerson.isSheetOwner = true;
                    arrSheetOwners[numberOfDirectConnections] = pPerson;
                    arrSheetOwners[numberOfDirectConnections].index = j;
                    numberOfDirectConnections++;
                }
                break;
            }
        }
    }
    console.log("Sheet owners: " + arrSheetOwners.length);
    return arrSheetOwners;
}

function extractUniqueCollaborators(p_iLoggedInUserId, sheets, people)
{
    numberOfIndirectConnections = 0;
    var arrSheetCollaborators = new Array();
    for (var i = 0; i < sheets.length; i++) {
        var pSheet = sheets[i];
        //Go through all the collaborators\
        for (j = 0; j < pSheet.collaborators.length; j++)
        {
            pCollaborator = pSheet.collaborators[j];
            var iSheetColloboratorId = pCollaborator.userId;
            //See if this collaborator is one of the people in the dataset
            for (var k = 0; k < people.length; k++) {
                pPerson = people[k];
                if (pPerson.userId == iSheetColloboratorId && pPerson.userId != p_iLoggedInUserId) {
                    //If this person hasn't already been marked as a sheet owner or a sheet collaborator, add them to the collaborators array
                    if ((pPerson.isSheetCollaborator == null || pPerson.isSheetCollaborator == false) && (pPerson.isSheetOwner == null || pPerson.isSheetOwner == false)) {
                        pPerson.isSheetOwner = false;
                        pPerson.isSheetCollaborator = true;
                        arrSheetCollaborators[numberOfIndirectConnections] = pPerson;
                        arrSheetCollaborators[numberOfIndirectConnections].index = k;
                        numberOfIndirectConnections++;
                    }
                    break;
                }
            }
        }
    }

    return arrSheetCollaborators;
}

function sortSheetsByName(p_arrSheets)
{
    if (arrSortedSheetsByName == null)
    {
        arrSortedSheetsByName = new Array();
    }
    arrSortedSheetsByName = p_arrSheets.sort(function (a, b) { return a.name - b.name; } )
}

function getSortedSheets()
{
    return sortSheetsByName;
}

function getNumberOfAttachments(p_arrSheets, p_iId)
{
    for (var i = 0; i < p_arrSheets.length; i++)
    {
        var pSheet = p_arrSheets[i];
        if (pSheet.id == p_iId)
        {
            if (pSheet.attachmentCount == null)
            {
                return 0;
            }
            else
            {
                return pSheet.attachmentCount;
            }
        }
    }
}