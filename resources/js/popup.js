﻿//Setup the popup
//0 means disabled; 1 means enabled;
var popupStatus = 0;

//loading popup with jQuery magic!
function loadPopup(popup, backgroundOpacity, fadeInSpeed){
    //loads popup only if it is disabled
    if (backgroundOpacity == null) backgroundOpacity = 1;
    if (fadeInSpeed == null) fadeInSpeed = "slow";
    if (popupStatus == 0) {
		$("#backgroundPopup").css("opacity", backgroundOpacity);
		$("#backgroundPopup").fadeIn(fadeInSpeed);
		$(popup).fadeIn(fadeInSpeed);
		popupStatus = 1;
	}
}

//disabling popup with jQuery magic!
function disablePopup(popup){
	//disables popup only if it is enabled
	if(popupStatus==1){
		$("#backgroundPopup").fadeOut("slow");
		$(popup).fadeOut("slow");
		popupStatus = 0;
	}
}

//Resets the popup flag and background but leaves the content in place
function resetPopupBackground() {
    $("#backgroundPopup").fadeOut("slow");
    popupStatus = 0;
}

//Set the popup background opacity
function setPopupOpacity(backgroundOpacity) {
    $("#backgroundPopup").css("opacity",backgroundOpacity);
    $("#backgroundPopup").fadeIn("fast");
    $("#backgroundPopup").fadeOut("fast");

}


//Resets the popup flag and popup but leaves the background in place
function resetPopup(popup) {
    $(popup).fadeOut("slow");
    popupStatus = 0;
}

//centering popup
function centerPopupOld(popup){
	//request data for centering
    var windowWidth = document.documentElement.clientWidth;
    windowWidth *= 1;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $(popup).height();
	var popupWidth = $(popup).width();
	//centering
	$(popup).css({
		"position": "absolute",
		"top": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}

function centerPopup(popup) {
    var $container = $("#play_main");
    var $clientArea = $(popup);

    screenHeight = window.innerHeight;
    screenWidth = window.innerWidth;

    var windowWidth = $container.width();
    var windowHeight = $container.height();
    var clientWidth = $clientArea.width();

    $clientArea.css({
        "position": "absolute",
        "left": windowWidth / 2 - clientWidth / 2
    });

}

//aligning popup
function alignPopup(popup, xadjustment,yadjustment){
	//request data for alignment
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $(popup).height();
	var popupWidth = $(popup).width();
	//centering
	$(popup).css({
		"position": "absolute",
		"top": windowHeight/2-popupHeight/2+yadjustment,
		"left": windowWidth/2-popupWidth/2+xadjustment
	});
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}

//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){

	/*	
	//LOADING POPUP
	//centering with css
	centerPopup("#introPopup");
	//load popup
	loadPopup("#introPopup",1,0);
	*/
/*	
	//CLOSING POPUP
	//Click the x event!
	$("#popupClose").click(function(){
		disablePopup("#introPopup");
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disablePopup("#introPopup");
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup("#introPopup");
		}
	});
*/
});