﻿//Globals
var configData;

//Define ConfiguationData Source Class
function configurationSource(uri) {
    this.json = null;
    this.failed = false;
    this.ready = false;

    this.read = function () {
        $.getJSON(uri)
          .done(function (json) {
              this.ready = true;
              this.failed = false;
              this.json = json;
              initData(this);
          })

          .fail(function (jqxhr, textStatus, error) {

              this.failed = true;
              this.ready = false;
              var err = textStatus + ", " + error;
              //console.log("Configuration.js:configurationSource.read - Request Failed: " + err);
          });
    }

}

function initConfiguration() {
  var configUri = "resources/configuration/Configuration.txt";
  configData = new configurationSource(configUri);
  configData.read();
}

function getConfigData()
{
    return configData.json;
}
