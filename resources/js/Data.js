﻿//Globals
var DEBUG_SHOW_JSON_DATA = true;

var dataUri = null;
var configData = null;

//Define Data Source Class
function dataSource() {

    if (DEBUG_USE_LOCAL_DATA == true)
    {
        sessionStorage["FetchBot3D.userEmail"] = "test@user.com";
        if (eval(sessionStorage["debug.dataset"]) == DEBUG_USE_SMALL_DATASET)
        {
          dataUri = "resources/data/small_data.txt";
        }
        else
        {
          dataUri = "resources/data/large_data.txt";
        }
    } else
    {
        dataUri = null;
    }

    this.json = null;
    this.sheets = null;
    this.people = null;
    this.failed = false;
    this.ready = false;

    this.read = function ()
    {
        if (dataUri == null)
        {
            var pJsonData = document.getElementById("jsonData");
            var strJsonData = pJsonData.value;
            if (DEBUG_SHOW_JSON_DATA)
            {
              console.log("Data Uri: " + dataUri);
              console.log("Json data: " + strJsonData);
            }
            var json = eval('(' + strJsonData + ')');
            this.ready = true;
            this.failed = false;
            this.json = json;
            this.sheets = json.sheets;
            this.people = json.users;
            //console.log("There are " + this.people.length + ' people');
            //console.log("There are " + this.sheets.length + ' sheets');
            try
            {
                startGame(this, configData);
            }
            catch(err)
            {
                console.log("Error: " + err);
                //window.location = ("NotSupported.html");
            }
        }
        else
        {
            $.getJSON(dataUri)
              .done(function (json)
              {
                  this.ready = true;
                  this.failed = false;
                  this.json = json;
                  this.sheets = json.sheets;
                  this.people = json.users;
                  try
                  {
                      startGame(this, configData);
                  }
                  catch (err)
                  {
                      console.log("Error: " + err);
                      //window.location = ("NotSupported.html");
                  }
              })
                    
              .fail(function (jqxhr, textStatus, error)
              {

                  this.failed = true;
                  this.ready = false;
                  var err = textStatus + ", " + error;
                  //console.log("Data.js:dataSource.read - Request Failed: " + err);
              });
        }
    }

}
function initData(config) {

    var data = new dataSource();
    configData = config;
    data.read();
}

